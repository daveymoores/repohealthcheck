import { json, urlencoded } from 'body-parser';
import { connect } from './utils/db';
import { signup, signin, protect } from './utils/auth';
import userRouter from './resources/user/user.router';
import repoRouter from './resources/repo/repo.router';
import * as express from 'express';
import * as cors from 'cors';
import * as morgan from 'morgan';
// import * as fs from 'fs';
// import * as https from 'https';

export const app = express();

app.disable('x-powered-by');

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(morgan('dev'));

app.use('/signup', signup);
app.use('/signin', signin);

app.use('/api', protect);
app.use('/api/user', userRouter);
app.use('/api/repo', repoRouter);

// const httpsOptions = {
//   key: fs.readFileSync('./certs/key.pem'),
//   cert: fs.readFileSync('./certs/cert.pem'),
// };

const PORT = process.env.PORT || 3000;

export const start = async () => {
  try {
    await connect();
    // https.createServer(httpsOptions, app).listen(PORT, () => {
    //   console.log(`REST API on https://localhost:${PORT}`);
    // });
    app.listen(PORT, () => {
      console.log(`REST API on http://localhost:${PORT}`);
    });
  } catch (err) {
    console.error(err);
    throw err;
  }
};
