import nodeFetch from 'node-fetch';
import { Request } from 'express';
import * as _ from 'lodash';
import { Repo } from '../../resources/repo/repo.model';
import { User } from '../../resources/user/user.model';

export interface Props {
  createdBy: string;
  repositoryUrl: string;
  hostingService: string;
}

export interface RepoTypes {
  [key: string]: string;
}

export interface Data {
  href: string;
}
export interface RepoData {
  url: string;
  slug: string;
}

const REPOS = (username: string): RepoTypes => ({
  github: `https://api.github.com/users/${username}/repos`,
  bitbucket: `https://api.bitbucket.org/2.0/users/${username}`,
});

const HEADERTYPES: RepoTypes = {
  github: 'application/vnd.github.v3+json',
  bitbucket: '',
};

// https://codeburst.io/javascript-async-await-with-foreach-b6ba62bbf404
async function asyncForEach(array: Props[], callback: any) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

export const fetchRepo = <T>(repo: RepoTypes, req: Request): Promise<T> => {
  const userId = req.params.id;

  return new Promise(async (resolve, _reject) => {
    let response: any = await nodeFetch(REPOS(repo.username)[repo.provider], {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: HEADERTYPES[repo.provider],
      },
    });

    console.log(response.status);
    if(response.status === 404) return;

    let repoData = await response.json();

    if (repo.provider === 'bitbucket') {
      // if bitbucket make two requests
      response = await nodeFetch(repoData.links.repositories.href, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      console.log(response.status);
      if(response.status === 404) return;

      repoData = await response.json();
    }

    const mappableData =
      repo.provider === 'bitbucket' ? repoData.values : repoData;

    const parsedRepoData = _.chain(mappableData)
      .reduce((acc: Props[], repoData: RepoData) => {
        const obj: Props = {
          repositoryUrl:
            repo.provider === 'bitbucket'
              ? `https://bitbucket.org/${repo.username}/${repoData.slug}`
              : repoData.url,
          createdBy: userId,
          hostingService: repo.provider,
        };
        acc.push(obj);
        return acc;
      }, [])
      .value();

    const start = async () => {
      await asyncForEach(parsedRepoData, async (item: Props) => {
        const repo = await Repo.create(item);
        await User.findByIdAndUpdate(
          userId,
          { $push: { repositories: repo._id } },
          { new: true },
        );
      });
    };

    await start();
    resolve();
  });
};

export const fetchRepoList = (req: Request): Promise<any> => {
  return Promise.all(
    _.chain(req.body)
      .map((repo: RepoTypes) => {
        return fetchRepo<Props[]>(repo, req);
      })
      .value(),
  );
};
