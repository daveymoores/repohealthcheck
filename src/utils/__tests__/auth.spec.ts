import { signup, verifyToken } from '../auth';
import { Response, Request } from 'express';
import { User } from '../../resources/user/user.model';

process.env.TEST_SUITE = 'auth-test';

describe('signup', () => {
  it('recieves no username or password for signup, returns 400 and error message', async () => {
    const req = {
      body: {},
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toBe(400);
        return this;
      },
      send(message: string) {
        expect(message).toEqual({
          message: 'please give a username and password',
        });
      },
    } as Response;

    await signup(req, res);
  });

  //https://medium.com/@art.longbottom.jr/concurrent-testing-with-mongoose-and-jest-83a27ceb87ee
  it('should recieve usename and password and return a user', async () => {
    const req = {
      body: {
        username: 'someuser',
        password: 'testpassword',
      },
    } as Request;

    const res = {
      status(status: string) {
        expect(status).toEqual(201);
        return this;
      },
      async send(result: { token: string }) {
        let user: any = await verifyToken(result.token);
        user = await User.findById(user.id)
          .lean()
          .exec();

        expect(user.username).toBe('someuser');
      },
    } as any;

    await signup(req, res);
  });

  it('tries to signup an existing user', async () => {
    const req = {
      body: {
        username: 'someuser',
        password: 'testpassword',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toBe(409);
        return this;
      },
      send(message: string) {
        expect(message).toEqual({
          message: 'Duplicate key error. User already exists.',
        });
      },
    } as Response;

    await signup(req, res);
  });
});
