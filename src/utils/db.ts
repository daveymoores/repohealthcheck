import * as mongoose from 'mongoose';
import { options } from '../config';

export const connect = (url: string = options.dbUrl) => {
  return mongoose.connect(url, { useNewUrlParser: true });
};
