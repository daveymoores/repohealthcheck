import { Response, Request, NextFunction } from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { User, UserProps } from '../resources/user/user.model';
import { options } from '../config';

export const newToken = (user: UserProps) => {
  return jwt.sign({ id: user.id }, options.secrets.jwt, {
    expiresIn: options.secrets.jwtExp,
  });
};

export const verifyToken = (token: string) =>
  new Promise((resolve, reject) => {
    jwt.verify(token, options.secrets.jwt, (err, payload) => {
      if (err) return reject(err);
      resolve(payload);
    });
  });

export const signup = async (req: Request, res: Response) => {
  if (!req.body.password || !req.body.username) {
    return res
      .status(400)
      .send({ message: 'please give a username and password' });
  }

  const userDetails = req.body;

  try {
    userDetails.password = bcrypt.hashSync(userDetails.password, 10);
    const user = await User.create(userDetails);
    const token = newToken(user);
    return res.status(201).send({ token });
  } catch (err) {
    console.error(err);
    return res
      .status(409)
      .send({ message: 'Duplicate key error. User already exists.' });
  }
};

export const signin = async (req: Request, res: Response) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!req.body.username || !req.body.password) {
    return res
      .status(400)
      .send({ message: 'please give a username and password' });
  }

  try {
    const user = await User.findOne({ username: username });
    if (!user) return res.status(404).send({ message: 'no user found' });

    const valid = bcrypt.compareSync(password, user.password.valueOf());

    if (!valid) {
      return res.status(400).send({ message: 'not auth' });
    }

    const token = newToken(user);
    return res.status(201).json({ token });
  } catch (err) {
    console.error(err);
    return res.status(401).end();
  }
};

export const protect = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  if (!req.headers.authorization) {
    return res.status(401).end();
  }

  let token = req.headers.authorization.split('Bearer ')[1];

  if (!token) {
    return res
      .status(401)
      .send({ message: 'no token in header' })
      .end();
  }

  try {
    const payload: any = await verifyToken(token);

    if ('id' in payload) {
      const user = await User.findById(payload.id)
        .select('-password')
        .lean()
        .exec();

      req.user = user;
      next();
    }
  } catch (err) {
    console.log(err);
    return res.status(401).end();
  }
};
