import { Request, Response } from 'express';
import { getUser, deleteUser } from '../user.controller';
import { User } from '../user.model';

process.env.TEST_SUITE = 'user-test';

describe('user controller', () => {
  test('tries to get a user without params and returns 400', async () => {
    expect.assertions(2);

    const req = {
      body: {
        username: '',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toBe(400);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({ message: 'no username on body' });
        return this;
      },
      end() {},
    } as Response;

    await getUser(req, res);
  });

  test('gets user object from username', async () => {
    expect.assertions(2);

    const mockUser = {
      username: 'daveymoores',
      password: 'password',
    };

    const newUser = await User.create(mockUser);

    const req = {
      body: {
        username: 'daveymoores',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toBe(200);
        return this;
      },
      send() {
        expect(newUser.id).toEqual(newUser.id);
        return this;
      },
      end() {},
    } as Response;

    await getUser(req, res);
  });

  test('deletes user and returns message', async () => {
    expect.assertions(2);

    const mockUser = {
      username: 'daveymoores',
      password: 'password',
    };

    const user = await User.create(mockUser);

    const req = {
      params: {
        id: user.id,
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toBe(200);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({ message: `user daveymoores deleted` });
        return this;
      },
      end() {},
    } as Response;

    await deleteUser(req, res, () => {});
  });
});
