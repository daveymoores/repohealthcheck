import { Schema, Model, model, Document, SchemaTypes } from 'mongoose';

export interface UserProps extends Document {
  username: string;
  password: string;
}

const userSchema: Schema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
  },
  repositories: [{ type: SchemaTypes.ObjectId, ref: 'Repo' }],
});

export const User: Model<UserProps> = model<UserProps>('User', userSchema);
