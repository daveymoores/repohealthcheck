import { User } from './user.model';
import { Request, Response, NextFunction } from 'express';

export const getUser = async (req: Request, res: Response) => {
  if (!req.body.username)
    return res.status(400).send({ message: 'no username on body' });

  try {
    const user = await User.findOne({ username: req.body.username }).populate(
      'repositories',
    );

    if (!user) return res.status(401).send({ message: 'no user found' });

    return res.status(200).send(user);
  } catch (err) {
    console.error(err);
    return res.status(400).end();
  }
};

export const updateUser = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  if (!Object.keys(req.body).length)
    return res.status(200).send({ message: 'No data given in body.' });

  try {
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    })
      .lean()
      .exec();

    if (!user) return res.status(401).send({ message: 'no user found' });

    res.status(200).send(user);
    next();
  } catch (err) {
    console.error(err);
    return res.status(400).end();
  }
};

export const deleteUser = async (
  req: Request,
  res: Response,
  _next: NextFunction,
) => {
  if (!req.params.id)
    return res
      .status(401)
      .send({ message: 'no params present. Please give a user id' });

  try {
    const user = await User.findByIdAndDelete(req.params.id);
    if (user)
      return res.status(200).send({ message: `user ${user.username} deleted` });
  } catch (err) {
    console.error(err);
    return res.status(400).end();
  }
};

const controllers = {
  updateUser: updateUser,
  getUser: getUser,
  deleteUser: deleteUser,
};

export default controllers;
