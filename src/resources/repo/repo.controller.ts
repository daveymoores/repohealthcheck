import * as _ from 'lodash';
import { Request, Response } from 'express';
import { Repo } from './repo.model';
import { User } from '../user/user.model';
import { fetchRepoList } from '../../utils/repos';

export const getRepos = async (req: Request, res: Response) => {
  // type Nullable<T> = T[] | null;
  let repo: any; //Nullable<RepoProps>;

  try {
    if (req.params.id) {
      repo = await Repo.findOne({ _id: req.params.id });
    } else {
      // need to add pagination here >>>
      repo = await Repo.find();
    }

    if (!repo || repo.length === 0)
      return res.status(404).send({ message: 'no repositories found' });

    return res.status(200).send(repo);
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const findRepos = async (req: Request, res: Response) => {
  if (!req.body.length || !req.params.id) {
    return res.status(401).send({ message: 'missing params or body data' });
  }

  try {
    await fetchRepoList(req);
    return res.status(200).send({ message: 'data added to repo model' });
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const deleteRepo = async (req: Request, res: Response) => {
  if (!req.params.id)
    return res.status(401).send({ message: 'no ID present in params' });

  const repoId = req.params.id;

  try {
    await Repo.findByIdAndDelete(repoId)
      .lean()
      .exec();
    return res.status(200).send({ message: 'repository deleted' });
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const updateRepo = async (req: Request, res: Response) => {
  if (!req.params.id)
    return res.status(401).send({ message: 'no ID present in params' });

  const repoId = req.params.id;

  try {
    const updatedRepo = await Repo.findByIdAndUpdate(repoId, req.body, {
      new: true,
    })
      .lean()
      .exec();

    return res.status(200).send(updatedRepo);
  } catch (err) {
    console.error(err);
    throw err;
  }
};

export const deleteAllReposForUser = async (req: Request, res: Response) => {
  if (!req.body.username)
    return res.status(401).send({ message: 'no username in body' });

  const username = req.body.username;

  try {
    const user = await User.findOne({ username });
    if (user) {
      await Repo.deleteMany({ createdBy: user._id });
      await User.findByIdAndUpdate(user._id, { repositories: [] });
      return res
        .status(200)
        .send({ message: `all repos deleted for user ${username}` });
    }

    return res.status(404).send({ message: 'user not found' });
  } catch (err) {
    console.error(err);
    throw err;
  }
};

const controllers = {
  getRepos: getRepos,
  findRepos: findRepos,
  deleteAllReposForUser: deleteAllReposForUser,
  deleteRepo: deleteRepo,
  updateRepo: updateRepo,
};

export default controllers;
