import { Request, Response } from 'express';
import {
  getRepos,
  updateRepo,
  deleteRepo,
  deleteAllReposForUser,
} from '../repo.controller';
import { Repo, RepoProps } from '../repo.model';
import { User } from '../../user/user.model';

process.env.TEST_SUITE = 'repo-test';

const createMockRepository = async () => {
  const mockUser = {
    username: 'testuser',
    password: 'testpassword',
  };

  const user = await User.create(mockUser);

  return {
    createdBy: user._id,
    repositoryUrl: 'https://someurl.com',
    repositoryName: 'repositoryname',
    hostingService: 'bitbucket',
  };
};

describe('repoRouter', () => {
  test('gets all repositories', async () => {
    expect.assertions(2);

    const mockRepo = await createMockRepository();
    await Repo.create(mockRepo);

    const req = {
      params: {},
    } as Request;

    const res = {
      status(status: Number) {
        expect(status).toEqual(200);
        return this;
      },
      send(repos: RepoProps) {
        expect(repos).toEqual(repos);
        return this;
      },
      end() {},
    } as Response;

    await getRepos(req, res);
  });

  test('get single repository', async () => {
    expect.assertions(2);

    const mockRepo = await createMockRepository();
    const repo = await Repo.create(mockRepo);

    const req = {
      params: {
        id: repo.id,
      },
    } as Request;

    const res = {
      status(status: Number) {
        expect(status).toEqual(200);
        return this;
      },
      send(repo: RepoProps) {
        expect(repo).toEqual(repo);
        return this;
      },
      end() {},
    } as Response;

    await getRepos(req, res);
  });

  test('tries to get all repos when none are present and returns 404', async () => {
    const req = {
      params: {},
    } as Request;

    const res = {
      status(status: Number) {
        expect(status).toEqual(404);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({ message: 'no repositories found' });
      },
    } as Response;

    await getRepos(req, res);
  });

  test('tries to amend a repo without params and returns 400', async () => {
    expect.assertions(2);

    const req = {
      params: {
        id: '',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toEqual(401);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({ message: 'no ID present in params' });
      },
    } as Response;

    await updateRepo(req, res);
  });

  test('tries to amend a repo and returns 200 and amended repo object', async () => {
    expect.assertions(2);

    const mockRepo = await createMockRepository();
    const repo = await Repo.create(mockRepo);

    const req = {
      params: {
        id: repo.id,
      },
      body: {
        repositoryUrl: 'https://adifferenturl.com',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toEqual(200);
        return this;
      },
      send(repo: RepoProps) {
        expect(repo.repositoryUrl).toEqual('https://adifferenturl.com');
      },
    } as Response;

    await updateRepo(req, res);
  });

  test("tries to delete repos for a user but returns 401 as username isn't present", async () => {
    expect.assertions(2);

    const mockRepo = await createMockRepository();
    await Repo.create(mockRepo);

    const req = {
      body: {
        username: '',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toEqual(401);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({ message: 'no username in body' });
      },
    } as Response;

    await deleteAllReposForUser(req, res);
  });

  test('deletes all repositories for user', async () => {
    expect.assertions(2);

    const mockRepo = await createMockRepository();
    await Repo.create(mockRepo);

    const req = {
      body: {
        username: 'testuser',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toEqual(200);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({
          message: `all repos deleted for user testuser`,
        });
      },
    } as Response;

    await deleteAllReposForUser(req, res);
  });

  test('deletes a single repository', async () => {
    expect.assertions(2);

    const mockRepo = await createMockRepository();
    const repo = await Repo.create(mockRepo);

    const req = {
      params: {
        id: repo._id,
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toEqual(200);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({ message: 'repository deleted' });
      },
    } as Response;

    await deleteRepo(req, res);
  });

  test('tries to delete a repo without params', async () => {
    expect.assertions(2);

    const req = {
      params: {
        id: '',
      },
    } as Request;

    const res = {
      status(status: number) {
        expect(status).toEqual(401);
        return this;
      },
      send(message: object) {
        expect(message).toEqual({ message: 'no ID present in params' });
      },
    } as Response;

    await deleteRepo(req, res);
  });
});
