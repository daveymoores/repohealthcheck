import { Router } from 'express';
import repoController from './repo.controller';

const router = Router();

router
  .route('/')
  .get(repoController.getRepos)
  .post(repoController.findRepos)
  .delete(repoController.deleteAllReposForUser);

router
  .route('/:id')
  .get(repoController.getRepos)
  .post(repoController.findRepos)
  .put(repoController.updateRepo)
  .delete(repoController.deleteRepo);

export default router;
