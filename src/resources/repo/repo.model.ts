import { Schema, Model, model, Document, SchemaTypes } from 'mongoose';

export interface RepoProps extends Document {
  createdBy: object;
  repositoryUrl: string;
  repositoryName: string;
  hostingService: string;
}

const repoSchema: Schema = new Schema(
  {
    createdBy: {
      type: SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    repositoryUrl: {
      type: String,
    },
    repositoryName: {
      type: String,
    },
    hostingService: String,
  },
  { timestamps: true },
);

export const Repo: Model<RepoProps> = model<RepoProps>('Repo', repoSchema);
