interface Options {
  dbUrl: string;
  secrets: {
    jwt: string;
    jwtExp: string;
  };
}

export const options: Options = {
  dbUrl: 'mongodb://localhost:27017/githubclitool',
  secrets: {
    jwt: process.env.JWT_SECRET || 'potato',
    jwtExp: '100d',
  },
};
